**************************
When do you need Inkscape?
**************************

There are many image creation and manipulation software programs.

However, there are two main types of images:

-  **Raster** or **bitmap** images are a majority of the digital images you generally encounter. Common examples are JPG, PNG, GIF, TIFF or BMP. Raster images are the default output of everything from digital cameras to most graphics editing software like Paint or Paintbrush. They are made up of :term:`pixels <Pixel>`, which is to say a grid of colored rectangular dots. 

   The advantage of raster graphics is that at high resolutions (lots of pixels in a small area) you can have very fine control over color and shape; raster images can contain millions of colors and look very realistic.

   However, the major downside to raster images is that the more the images are manipulated after creation, the more the quality declines. For instance, if you create your image at one size, making it bigger can create jagged-looking edges ("pixelation"), while shrinking it can lead to colors and shapes blurring into each other.

-  **Vector** images are created with vector graphics software (like Inkscape). While Inkscape uses an internationally standardized format for two-dimensional vector graphics (SVG), many other, sometimes proprietary, file formats for vector graphics and their various applications (fonts, CAD, cartography, 3D modeling …) exist. Instead of being made of dots, vector images are made of mathematically-defined lines and curves.

   Lines in a vector graphic can also be joined together to form larger :term:`objects <Object>`, each of which can be given their own style (colors, patterns, etc.).

   Because all of the elements of the vector image are defined by mathematical formulae and not by a grid of dots as in a raster image, sharpness is maintained regardless of how much the image is enlarged. However large or small you make it, the curves are simply recalculated and redrawn, without losing quality.

   As a result, a single vector image can be used for different sizes of the final image presentation. For example, the same image will look equally sharp and clear -- and have exactly the same colors and proportions -- on both a business card and a large poster.

   One downside is that, if used for "realistic" drawing, vector images can have a too-clean, artificial look.

Depending on how you intend to use your image, either raster or vector graphics may better suit your needs.

**Inkscape** is a **program for creating and editing vector graphics**.

It is the ideal tool for drawing logos and icons, creating (animatable) graphics
for websites, for composing posters and flyers, or for making patterns for use
with cutting machines and laser engravers.

It is also a good choice for drawing short comics, making art, building user
interface mockups and making or editing diagrams created by other software.
People use it for fun projects, such as map-making, creating birthday cards,
painting on eggs, 2D game graphics and for **many, many more purposes**.

Inkscape is **not the ideal tool** for some other purposes, but there exist very
powerful alternative free software programs which we recommend without
reservations:

- **Photo editing** (raster graphics): `Gimp <https://gimp.org>`_
- **Desktop publishing** (multi-page documents, printing in CMYK color space): `Scribus <https://scribus.net>`_
- **CAD** (parametrical construction, engineering): `LibreCAD (2D) <https://librecad.org/>`_
- **Painting** (raster graphics): `Krita <https://krita.org>`_
- **3D modeling** (three-dimensional graphics): `Blender <https://blender.org>`_

Here are some examples of the types of images and projects that can be made with Inkscape:

Artwork and Comics
==================

.. image:: images/examples/WINTERFALL_by_Rizki_Ilman_Hidayanto_DzunnunSulaiman-PD.png
   :width: 22%
.. image:: images/examples/inkscape-island-of-creativity_by_Bayu_Rizaldhan_Rayes_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/COCOART_by_Rizki_Ilman_Hidayanto_DzunnunSulaiman-PD.png
   :width: 22%
.. image:: images/examples/InkboardDoF_by_Piotr_Milczanowski-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/InkscapeMan_Final_by_Tim_Jones-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/cartoon-092_by_GD-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/snail_the_alien_by_Danang_Bimantoro-PD.png
   :width: 22%
.. image:: images/examples/Elvie_056_en-GB_www.peppertop.com_CC-BY-SA.jpg
   :width: 22%


Clip art, Icons and Logos
=========================

.. image:: images/examples/baby-narwhal_Martin_Owens_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/bug_induced_by_Alexey_Kyurshunov-CC_BY.png
   :width: 22%
.. image:: images/examples/Spell_Book_Equinox3141_CC-BY-SA.png
   :width: 22%
.. image:: images/examples/bug_documentation_by_Alexey_Kyurshunov-CC_BY.png
   :width: 22%
.. image:: images/examples/lucentis_by_Cristian_Pozzessere_CC_By_SA.png
   :width: 22%
.. image:: images/examples/x_by_theddy_grumo_PD.png
   :width: 22%
.. image:: images/examples/Moon_by_desmafagafado_PD.png
   :width: 22%
.. image:: images/examples/logo_stamp_r02_CC_BY_SA.png
   :width: 22%

Layout and Prototyping
======================

.. image:: images/examples/FreiesMagazin_April_2015_by_Maren_Hachmann-CC_BY_SA.jpg
   :width: 22%
.. image:: images/examples/slide-gnu_Roger_Condo_Ochoa_CC-BY.png
   :width: 22%
.. image:: images/examples/Gimp_Inkscape_Banners_by_Ryan_Gorley_CC_By-SA.jpg
   :width: 22%
.. image:: images/examples/0002-BLOG_IDA-MUZAIKO_Eder_Benedetti_CC-BY.png
   :width: 22%
.. image:: images/examples/Concept_art_pack_Mypaint_Ramon_Miranda_PD.png
   :width: 22%
.. image:: images/examples/telescopica_mobile_by_Marien_Soria-CC_BY_SA.png
   :width: 22%
.. image:: images/examples/UI_Elements_hayavadhan-CC_BY.png
   :width: 22%
.. image:: images/examples/Logo_RM_and_tshirt_by_Ramon_Miranda_CC_By_SA.png
   :width: 22%

Technical Drawings and Diagrams
===============================

.. image:: images/examples/Mohrs_circle_by_Johannes_Kalliauer-PD.png
   :width: 22%
.. image:: images/examples/mtDNA_by_sdjbrown-CC_BY.png
   :width: 22%
.. image:: images/examples/Origami_Tux_by_Dario_Badagnani-CC-BY-SA.jpg
   :width: 22%
.. image:: images/examples/croquis_afrique_sans_nom_by_flanet-PD.png
   :width: 22%

Physical Objects
================

.. image:: images/examples/Commodore_VC1520_plotter_extension_for_Inkscape_by_Johan_Van_den_Brande_MIT.jpg
   :width: 22%
.. image:: images/examples/cookiecutter_by_Alexander_Pruss_MIT.png
   :width: 22%
.. image:: images/examples/Widgets_Paris_by_Martin_Owens_CC_BY_SA.JPG
   :width: 22%
.. image:: images/examples/i_heart_vectors_by_Lex_Neva_CC-BY-SA_4.0.jpg
   :width: 22%


.. image:: images/vector-format.png
   :class: deco center medium
