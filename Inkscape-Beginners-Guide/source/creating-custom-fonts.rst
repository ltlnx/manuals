*********************
Creating Custom Fonts
*********************

Fonts can be saved in different file formats: OTF, TTF and SVG (and some more).

With Inkscape, you can create :term:`SVG fonts <SVG font>`. 

These fonts are embedded in the resulting SVG file. For being able to use them in a text editor or graphics program, however, they must later be converted to another font file format.

You will not be able to use the SVG file directly as a font in a webpage, for instance.
This is because SVG fonts have been removed from the official SVG standard.
However, they are useful for font-making in Inkscape, and other font-making programs can still convert them to other font file formats.

The letters, numbers and special characters
a font consists of are called :term:`glyphs <Glyph>`. 

To create a your custom SVG font:

#. Open the typography template with :menuselection:`File --> New from Template --> Typography Canvas`.
#. Open the :guilabel:`Font Editor` dialog from :menuselection:`Text --> SVG Font Editor`.
#. In the column labelled :guilabel:`Font`, click on :guilabel:`New` to create a font. You can double-click on the generic name, in this case :guilabel:`font 1`, of the font to change it.
#. Open the :guilabel:`Layers` dialog from :menuselection:`Layer --> Layers`.

.. Tip::
  You can also use the Layers dialog button in the commands bar.

Repeat the following for each glyph that you want to add to your font:

#. In the :guilabel:`Layers` dialog, add a new layer by clicking on the '+' icon.
   Name it after your letter. Select the layer in the dialog.
#. Now, in the :guilabel:`Font Editor`, in the tab :guilabel:`Glyphs`, click on
   :guilabel:`Add Glyph`. Double-click on the :guilabel:`Glyph name` field to
   name your glyph, e.g. call it 'Capital A' or 'Space'. In the
   :guilabel:`Matching String` field, enter the letter that it corresponds to.
#. Draw the path for your glyph on the canvas. It must be a closed, 
   single path, which may consist of multiple subpaths.
#. When you're happy with your glyph, select it, and also select the
   corresponding row in the dialog, then click on :guilabel:`Get curves from
   selection`.

.. Tip::
  You can always test your font by typing a text into the field at the bottom of the :guilabel:`SVG Font Editor` dialog and looking at the preview above it.

.. Hint::
  You can use the little 'eye' icons in the :guilabel:`Layers` dialog to hide the layers with the glyphs that you have already finished. To protect the completed glyph layers from accidental changes, use the 'lock' icons.

When you are done making the glyphs, save the file as an Inkscape SVG (this is Inkscape's standard file format).

Although this functionality is meant for typographers, amateurs, too, can quickly
get a working result and test their work as they continue.

When your font is finished, you can use a font-making software like `FontForge
<https://fontforge.github.io/>`_ where you import your SVG font and can export
it into other different formats to be able to use it with other software, like Microsoft Word, LibreOffice, in any other program that supports the choice of fonts, or in a HTML file using CSS.

.. figure:: images/font_editor.png
   :alt: Typography canvas
   :class: screenshot

   The typography template has just the right size for a single letter. It comes with 5 useful guides for making your font. On the right side, the (lowercase) letter 'a' has already been added to the SVG font.
