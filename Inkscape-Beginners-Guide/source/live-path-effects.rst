*****************
Live Path Effects
*****************

.. Inkscape version 1.3+

**Live Path Effects** (or LPEs) in Inkscape can be used to create new exciting objects from paths and groups, without destroying the original paths.

They can be selected and adjusted in the :guilabel:`Path Effects` dialog, which you can access as :guilabel:`Path Effects` from the bottom of the :guilabel:`Path` menu entry.

Each type of path effect has its own, unique set of options in this dialog, and most of them can also be manipulated by dragging the effect's handles with the Node tool on canvas.

Path effects can also be stacked. So you could, for example, first roughen the outline of a path, then arrange copies of the path around a circle, and then align dozens of those circles of the roughened paths in a grid. 

Since these effects are non-destructive, you can edit your original shape any time later, and all the stacked effects will automatically adjust to the changes.

Adding a Live Path Effect (LPE) to a Path
=========================================

#. Select an object.
#. Open the LPE dialog (:menuselection:`Path --> Live Path Effects`).
#. In the LPE dialog, click the arrow in the search field or start typing to see all available effects.
#. Choose an effect from the list.
#. Adjust options in the dialog. Handles appear next to the object for further adjustments if you use Node Tool (N).

Effects will instantly alter the appearance of your path.


Examples
========

Here's a description of some of the available LPEs. Don't hesitate to discover the functionality of the others on your own:


Bend
  This effect allows you to take any shape and bend it along a custom path. Useful for creating custom brushes or deforming objects on a path.

Corners (Fillet/Chamfer)
  After you add this effect, you gain the ability to round any corner on your shape. You can round individual nodes by selecting them and dragging green handles on the canvas, or you can change values in the dialog.

Hatches
  Fills your shape with hatches that can be heavily customized.

Interpolate Sub-Paths
  The principle is the same as for the :guilabel:`Interpolate` extension, but the LPE approaches the topic differently. It will not create intermediate forms between two different objects, but only between parts of a single path. You need to combine two paths, so you have a path consisting of two sub-paths for this effect to work (:menuselection:`Path --> Combine` or :kbd:`Ctrl` + :kbd:`K`). Use the options to control the number of in-between steps and the trajectory along which blending will happen.

Knot
  Creates gaps in sections of a path, when they cross with another part of the path.

Mirror Symmetry
  This effect mirrors your drawing along an axis of your choice. You could try it when drawing symmetrical objects like faces, bottles, buildings, or butterflies.

Perspective / Envelope
  Seriously helpful for drawing in perspective, thanks to the addition of 4 handles for the corners that can be moved around to make it look like a flat object is tilted in 3D space.

Stitch Sub-Paths
  This effect is similar to Interpolate Sub-Paths, but it adds perpendicular lines between two paths.

Pattern Along Path
  This LPE's principle is similar to the extension with the same name. However, here the pattern parts will all be parts of the same path (and they will always be distorted to follow the path direction). It can be used like a brush, or just for adding a repeating edge to any object.

Offset
  This effect changes the thickness of the whole object. You can change the offset (or inset, too!) with a specific value in a panel. Or you can drag on a round handle on the canvas just using your mouse.

Power Stroke
  Adds handles that allow you to change the width of the path, so it can be different at different locations along the path.

Rotate Copies
  This effect creates many copies of the path or object. It places those copies of the object around a circle. You could use it for making mandalas, flowers, and other radial drawings.

Roughen
  This effect adds imperfections to your shapes to simulate a more organic drawing.

Sketch
  Transforms your path into multiple lines that look like a sketch drawn with a pencil.

Tiling
  This is a very powerful tool for quickly creating and arranging many copies of an object. It offers options to control the number of clones/tiles in rows and columns, gaps between them, offsets, size and rotation. There are also 16 different tiling rules available for you to choose from. This is useful for creating patterns, grids, and other repeating elements.

.. TODO needs images of the LPEs

Conclusion
==========

Now we have taken a small tour around many of the effects that add functionality to Inkscape and allow you to automate specific tasks.

This book is only an introduction to Inkscape. Do not hesitate to further explore the software once you feel at ease with its basic functionality.
