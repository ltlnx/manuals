sphinx==5.0.1
sphinx-intl==2.1.0
sphinx-rtd-theme==1.0.0
python-Levenshtein==0.23.0
